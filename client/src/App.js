import React, { useState, useEffect } from "react";
import axios from "axios";
import "./App.css";
import 'react-app-polyfill/ie9';
import 'react-app-polyfill/ie11';

import 'react-app-polyfill/stable';


function App() {
  const [state, setState] = useState({
    data: [{ index: 0, completed: true, task: "", editing: false }],
  });
  useEffect(() => {
    updateData();
  },[]);


  const stateSetter = (arr) =>{

    arr.sort((a, b) => (a.index > b.index ? 1 : -1));

    setState({data:arr});
  }

  const updateData = async () => {
    const x = await axios.get("/api/todo/");
    stateSetter(x.data.data);
    console.log(x.data.data[0].editing )
  };



  const insertTask = async (e) => {
    e.preventDefault();
    const payload = {
      index: state.data.length,
      completed: false,
      editing:false,
      task: document.getElementById("taskInput").value,
    };
    const x = await axios.post("/api/todo/", payload);
    stateSetter(x.data.data);
    document.getElementById("form").reset();
  };

const deleteTask = async (id) => {
    const x = await axios.delete("/api/todo/" + id);
    stateSetter(x.data.data);

  };

  const changeCompletionStatus = async (id, index) => {
      const payload = {
      id: id,
      completed: !state.data[index].completed
    };
    const x = await axios.post("/api/todo/status", payload);

    stateSetter(x.data.data);
  };

  
  const raiseTaskIndex = async (index) => {
    if (index === 0) {
      return;
    }
    var id1 = state.data[index]._id
    var index1 = state.data[index].index -1
    var id2 = state.data[index-1]._id
    var index2 = state.data[index-1].index + 1
console.log(id1,index1, id2, index2)
    const payload = {
      id1: id1,
index1: index1,
id2: id2,
index2:index2    };

    const {data} = await axios.post("/api/todo/order", payload);
        
   stateSetter(data.data);

  };
  const lowerTaskIndex = async (index) => {
    if (index === state.data.length - 1) {
      return;
    }
    var id1 = state.data[index]._id
    var index1 = state.data[index].index +1
    var id2 = state.data[index+1]._id
    var index2 = state.data[index+1].index - 1
console.log(id1,index1, id2, index2)
    const payload = {
      id1: id1,
index1: index1,
id2: id2,
index2:index2    };

    const {data} = await axios.post("/api/todo/order", payload);

   stateSetter(data.data);

  };

  const editMode = async (index)=>{
   
    const payload = { id: state.data[index]._id, editing: !state.data[index].editing}

    const {data} = await axios.post("/api/todo/mode", payload);
console.log(data)
   stateSetter(data.data);
  }


  const editTask = async (index)=>{
   
    const payload = { id: state.data[index]._id, task: document.getElementById("editTitle"+index).value  }

    const {data} = await axios.post("/api/todo/edit", payload);
console.log(data)
   stateSetter(data.data);
  }
 
  return (
    <div className="pageHolder">
      <div className="title">Your Personal To-Do List!</div>

      {state.data.map((object, index) => (
        <div className="totalTodo">
          <div className="deleteButton" onClick={() => deleteTask(object._id)}>
            X
          </div>
          <div className="arrow" onClick={() => raiseTaskIndex(index)}>
            ↑
          </div>
        <div  className="inputHolder" style = {{padding:"0"}}>
          {object.editing === false &&<div
            className="task"
            style={
              object.completed !== false
                ? { textDecoration: "line-through" }
                : { textDecoration: "none" }
            }
          >
            {object.task}
          </div>}
          {object.editing === true && <input type="text" id = {"editTitle"+index} defaultValue = {object.task} onChange ={() => editTask(index)} />}
          
          <div onClick={() => editMode(index)} style = {{cursor: "pointer"}}>{object.editing === true ? "✓" : "✎"}</div>
          </div>
          <div className="arrow" onClick={() => lowerTaskIndex(index)}>
            ↓
          </div>
          <div
            className="doneButton"
            style={
              object.completed === true
                ? { backgroundColor: "yellow" }
                : { backgroundColor: "green" }
            }
            onClick={() => changeCompletionStatus(object._id, index)}
          >
            {object.completed === false ? "✓" : "↺"}
          </div>
        </div>
      ))}

      <form id="form" onSubmit={(e) => insertTask(e)}>
        <div className="inputHolder">
          <input
            id="taskInput"
            type="text"
            className="input"
            placeholder="Enter task here"
            required
          />
          <input id="submitButton" type="submit" />
        </div>
      </form>
    </div>
  );
}

export default App;
