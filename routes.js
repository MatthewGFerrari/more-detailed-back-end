const express = require("express");
const Model = require("./model");
const router = express.Router();

const {getData} = require ("./proof")

router.get("/", (req, res) => getData(req,res));

var postData = async (req, res) => {
  upload = await Model.create(req.body);
  total = await Model.find();
  return res.status(200).json({ data: total });
};

router.post("/", (req, res) => postData(req, res));

var deleteData = async (req, res) => {
  try {
    find = await Model.findById(req.params.id);
  } catch (err) {
      Console.log(err.name)
    return res.status(200).json({ data: "that likely doesnt exist" });
  }

  find.remove()

  list = await Model.find();
  return res.status(200).json({ data: list });
};

router.delete("/:id", (req, res) => deleteData(req, res));

var updateOrder = async (req, res) => {
    var {id1, index1,id2, index2} = req.body
    console.log('we out here')
    try {
      find = await Model.findById(id1);
      find2 = await Model.findById(id2);

    } catch (err) {
        Console.log(err.name)
      return res.status(200).json({ data: "at least one of those likely doesnt exist" });
    }
  
    query = await Model.updateOne({_id: id1 }, {$set: {index: index1}})
    query2 = await Model.updateOne({_id: id2 }, {$set: {index: index2}})

    list = await Model.find();
    return res.status(200).json({ data: list });


  };

//id1

router.post("/order", (req, res) => updateOrder(req, res));


var updateStatus = async (req, res) => {
    var {id, completed} = req.body
    try {
      find = await Model.findById(id);

    } catch (err) {
        Console.log(err.name)
      return res.status(200).json({ data: "That likely doesnt exist" });
    }
  
    query = await Model.updateOne({_id: id }, {$set: {completed: completed}})

    list = await Model.find();
    return res.status(200).json({ data: list });


  };

//id1

router.post("/status", (req, res) => updateStatus(req, res));

var editTask = async (req, res) => {
    var {id, task} = req.body
    try {
      find = await Model.findById(id);

    } catch (err) {
        Console.log(err.name)
      return res.status(200).json({ data: "That likely doesnt exist" });
    }
  
    query = await Model.updateOne({_id: id }, {$set: {task: task}})

    list = await Model.find();
    return res.status(200).json({ data: list });


  };

//id1

router.post("/edit", (req, res) => editTask(req, res));


var editMode = async (req, res) => {
    var {id, editing} = req.body
    try {
      find = await Model.findById(id);

    } catch (err) {
        Console.log(err.name)
      return res.status(200).json({ data: "That likely doesnt exist" });
    }
  
    query = await Model.updateOne({_id: id }, {$set: {editing: editing}})

    list = await Model.find();
    return res.status(200).json({ data: list });


  };

//id1

router.post("/mode", (req, res) => editMode(req, res));

module.exports = router;
