const express = require("express")
const routes = require("./routes")
const db = require("mongoose")
const app = express()

app.use(express.json())
app.use("/api/todo", routes)
db.connect("mongodb+srv://me:me@cluster0-ejvgl.mongodb.net/test?retryWrites=true&w=majority", {useNewUrlParser: true, useUnifiedTopology: true },() => console.log("we connected") )
app.get(("/"), (req,res) => res.send("hello world mama I made it"))

app.listen(5000, () => console.log("serving on port 5000"))