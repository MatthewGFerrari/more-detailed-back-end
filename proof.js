const express = require("express");
const Model = require("./model");
const router = express.Router();

 getData = async (req, res) => {
  data = await Model.find();
  return res.status(200).json({ data: data });
};
module.exports = {getData}