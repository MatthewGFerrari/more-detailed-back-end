const mongoose = require("mongoose")

const PostSchema = new mongoose.Schema({

    index:{
        type:Number,
        required:true
    },
    completed:{
        type:Boolean,
        required:true
    },
    editing:{
        type:Boolean,
        required:true
    },
    task:{
        type:String,
        required:true
    }
})

module.exports = mongoose.model('Model', PostSchema)